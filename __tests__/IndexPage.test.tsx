import React from "react";
import { act, render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import IndexPage, { PLANNING_PLACEHOLDER_TEXT } from "../components/IndexPage";
import WORKING_STATUSES from "../state/WorkingStatus";

test("renders mode toggles", () => {
  act(() => {
    render(<IndexPage />);
  });
  let buttonElement: HTMLElement = screen.getByText(/short break/i);
  expect(buttonElement).toBeInTheDocument();

  buttonElement = screen.getByText(/long break/i);
  expect(buttonElement).toBeInTheDocument();

  buttonElement = screen.getByText(/pomodoro/i);
  expect(buttonElement).toBeInTheDocument();
});

test("renders start time (25 minutes)", () => {
  act(() => {
    render(<IndexPage />);
  });
  const textElement: HTMLElement = screen.getByText(/25:00/i);
  expect(textElement).toBeInTheDocument();
});

test("renders action buttons", () => {
  act(() => {
    render(<IndexPage />);
  });
  let buttonElement: HTMLElement = screen.getByText(/start/i);
  expect(buttonElement).toBeInTheDocument();
  buttonElement = screen.getByText(/pause/i);
  expect(buttonElement).toBeInTheDocument();
  buttonElement = screen.getByText(/reset/i);
  expect(buttonElement).toBeInTheDocument();
});

test("renders status", () => {
  act(() => {
    render(<IndexPage />);
  });
  const statusElement: HTMLElement = screen.getByText(/status/i);
  expect(statusElement).toBeInTheDocument();
});

test("toggle button changes status from planning to working and back", () => {
  act(() => {
    render(<IndexPage />);
  });
  const statusElement: HTMLElement = screen.getByText(
    WORKING_STATUSES.notWorking
  );
  const startButton: HTMLElement = screen.getByText(/start/i);
  startButton.click();

  const planningInput: HTMLTextAreaElement = screen.getByPlaceholderText(
    PLANNING_PLACEHOLDER_TEXT
  ) as HTMLTextAreaElement;
  userEvent.type(planningInput, "Finish example project");

  const toggleButton: HTMLElement = screen.getByText(/toggle/i);
  toggleButton.click();

  expect(statusElement).toHaveTextContent(WORKING_STATUSES.working);
  toggleButton.click();
  expect(statusElement).toHaveTextContent(WORKING_STATUSES.planning);
});

test("pause button changes status from working / planning to paused", () => {
  act(() => {
    render(<IndexPage />);
  });
  const startButton: HTMLElement = screen.getByText(/start/i);
  const pauseButton: HTMLElement = screen.getByText(/pause/i);
  const statusElement: HTMLElement = screen.getByText(
    WORKING_STATUSES.notWorking
  );
  startButton.click();
  pauseButton.click();
  expect(statusElement).toHaveTextContent(WORKING_STATUSES.paused);
});

test("pause button does not change status from not working", () => {
  act(() => {
    render(<IndexPage />);
  });
  const statusElement: HTMLElement = screen.getByText(
    WORKING_STATUSES.notWorking
  );
  const pauseButton: HTMLElement = screen.getByText(/pause/i);
  pauseButton.click(); // Nothing happens
  expect(statusElement).toHaveTextContent(WORKING_STATUSES.notWorking);
});

test("start button changes status from paused to planning", () => {
  act(() => {
    render(<IndexPage />);
  });
  const pauseButton: HTMLElement = screen.getByText(/pause/i);
  const startButton: HTMLElement = screen.getByText(/start/i);
  const statusElement: HTMLElement = screen.getByText(
    WORKING_STATUSES.notWorking
  );
  pauseButton.click();
  startButton.click();
  expect(statusElement).toHaveTextContent(WORKING_STATUSES.defaultDirty);
});

test("start button doesn't change status from planning", () => {
  act(() => {
    render(<IndexPage />);
  });
  const startButton: HTMLElement = screen.getByText(/start/i);
  const statusElement: HTMLElement = screen.getByText(
    WORKING_STATUSES.notWorking
  );
  startButton.click(); // => Planning
  startButton.click(); // Nothing Happened
  expect(statusElement).toHaveTextContent(WORKING_STATUSES.planning);
});

test("start button doesn't change status from working", () => {
  act(() => {
    render(<IndexPage />);
  });
  const startButton: HTMLElement = screen.getByText(/start/i);
  const statusElement: HTMLElement = screen.getByText(
    WORKING_STATUSES.notWorking
  );
  startButton.click(); // => Planning

  const planningInput: HTMLTextAreaElement = screen.getByPlaceholderText(
    PLANNING_PLACEHOLDER_TEXT
  ) as HTMLTextAreaElement;
  userEvent.type(planningInput, "Finish example project");

  const toggleButton: HTMLElement = screen.getByText(/toggle/i);
  toggleButton.click(); // => Working
  startButton.click(); // Nothing happened
  expect(statusElement).toHaveTextContent(WORKING_STATUSES.working);
});

test("reset button doesn't change status from paused", () => {
  act(() => {
    render(<IndexPage />);
  });
  const resetButton: HTMLElement = screen.getByText(/reset/i);
  const statusElement: HTMLElement = screen.getByText(
    WORKING_STATUSES.notWorking
  );
  resetButton.click(); // Nothing happens
  expect(statusElement).toHaveTextContent(WORKING_STATUSES.notWorking);
});

test("reset button changes status from planning to not working", () => {
  act(() => {
    render(<IndexPage />);
  });
  const resetButton: HTMLElement = screen.getByText(/reset/i);
  const startButton: HTMLElement = screen.getByText(/start/i);
  const statusElement: HTMLElement = screen.getByText(
    WORKING_STATUSES.notWorking
  );
  startButton.click(); // => Planning
  resetButton.click(); // => Not Working
  expect(statusElement).toHaveTextContent(WORKING_STATUSES.notWorking);
});

test("reset button changes status from working to not working", () => {
  act(() => {
    render(<IndexPage />);
  });
  const resetButton: HTMLElement = screen.getByText(/reset/i);
  const startButton: HTMLElement = screen.getByText(/start/i);
  const statusElement: HTMLElement = screen.getByText(
    WORKING_STATUSES.notWorking
  );
  startButton.click(); // => Planning
  const toggleButton: HTMLElement = screen.getByText(/toggle/i);
  toggleButton.click(); // => Working
  resetButton.click(); // => Not Working
  expect(statusElement).toHaveTextContent(WORKING_STATUSES.notWorking);
});

test("toggle button should not be visible initally", () => {
  act(() => {
    render(<IndexPage />);
  });
  const toggleButton: HTMLElement | null = screen.queryByText(/toggle/i);
  expect(toggleButton).toBeNull();
});

test("toggle button should be visible if working / planning", () => {
  act(() => {
    render(<IndexPage />);
  });
  const startButton: HTMLElement = screen.getByText(/start/i);
  startButton.click(); // => Planning
  const toggleButton: HTMLElement = screen.getByText(/toggle/i);
  expect(toggleButton).toBeInTheDocument();
});

test("toggle button should not be visible if paused", () => {
  act(() => {
    render(<IndexPage />);
  });
  const startButton: HTMLElement = screen.getByText(/start/i);
  const pauseButton: HTMLElement = screen.getByText(/pause/i);
  startButton.click(); // => Planning
  pauseButton.click(); // => Paused
  const toggleButton: HTMLElement | null = screen.queryByText(/toggle/i);
  expect(toggleButton).toBeNull();
});

test("start button should change the timer", (done) => {
  act(() => {
    render(<IndexPage />);
  });
  const startButton: HTMLElement = screen.getByText(/start/i);
  startButton.click(); // => Planning
  setTimeout(() => {
    // TODO: Use async methods instead of this hack
    try {
      const particularTime25Min: HTMLElement | null =
        screen.queryByText(/25:00/i);
      expect(particularTime25Min).toBeNull();
      done();
    } catch (error) {
      done(error);
    }
  }, 2000); // NOTE: Wait more than a second... for some reason...
});

test("planning input should be visible if working / planning", () => {
  act(() => {
    render(<IndexPage />);
  });
  const startButton: HTMLElement = screen.getByText(/start/i);
  startButton.click(); // => Planning
  const planningInput: HTMLElement = screen.getByPlaceholderText(
    "What are you working on...?"
  );
  expect(planningInput).toBeInTheDocument();
});

test("planning input should not be readonly if planning", () => {
  act(() => {
    render(<IndexPage />);
  });
  const startButton: HTMLElement = screen.getByText(/start/i);
  startButton.click(); // => Planning
  const planningInput: HTMLElement = screen.getByPlaceholderText(
    "What are you working on...?"
  );
  expect(planningInput).not.toHaveAttribute("readonly");
});

test("planning input should be readonly if working", () => {
  act(() => {
    render(<IndexPage />);
  });
  const startButton: HTMLElement = screen.getByText(/start/i);
  startButton.click(); // => Planning

  const planningInput: HTMLTextAreaElement = screen.getByPlaceholderText(
    PLANNING_PLACEHOLDER_TEXT
  ) as HTMLTextAreaElement;
  userEvent.type(planningInput, "Finish example project");

  const toggleButton: HTMLElement = screen.getByText(/toggle/i);
  toggleButton.click();
  expect(planningInput).toHaveAttribute("readonly");
});

test("toggle button should be disabled if planning input is empty", () => {
  act(() => {
    render(<IndexPage />);
  });
  const statusElement: HTMLElement = screen.getByText(
    WORKING_STATUSES.notWorking
  );
  const startButton: HTMLElement = screen.getByText(/start/i);
  startButton.click(); // => Planning
  const planningInput: HTMLTextAreaElement = screen.getByPlaceholderText(
    "What are you working on...?"
  ) as HTMLTextAreaElement;
  const toggleButton: HTMLElement = screen.getByText(/toggle/i);
  planningInput.value = "Example project";
  toggleButton.click(); // => Does nothing
  expect(statusElement).toHaveTextContent(/planning/i); // button shouldn't work
  expect(toggleButton).toHaveAttribute("disabled"); // button should be disabled
});

test("Timer changes to 5 minutes on clicking short break", () => {
  act(() => {
    render(<IndexPage />);
  });
  let timer: HTMLElement = screen.getByText(/25:00/i); // Look for 05:00, NOT 25:00
  const shortBreakButton: HTMLElement = screen.getByText(/short break/i);
  shortBreakButton.click();

  setTimeout((done) => {
    // TODO: Use async methods instead of this hack
    try {
      timer = screen.getByText(/05:00/i);
      expect(timer).toBeInTheDocument();
      done();
    } catch (error) {
      done(error);
    }
  }, 2000); // NOTE: Wait more than a second...
});

test("Timer changes to 15 minutes on clicking long break", () => {
  act(() => {
    render(<IndexPage />);
  });
  let timer: HTMLElement = screen.getByText(/25:00/i);
  const longBreakButton: HTMLElement = screen.getByText(/long break/i);
  longBreakButton.click();

  setTimeout((done) => {
    // TODO: Use async methods instead of this hack
    try {
      timer = screen.getByText(/15:00/i);
      expect(timer).toBeInTheDocument();
      done();
    } catch (error) {
      done(error);
    }
  }, 2000); // NOTE: Wait more than a second...
});

test("Timer does not run immediately on clicking short break", () => {
  act(() => {
    render(<IndexPage />);
  });
  const shortBreakButton: HTMLElement = screen.getByText(/short break/i);
  shortBreakButton.click();

  setTimeout((done) => {
    // TODO: Use async methods instead of this hack
    try {
      const particularTime25Min: HTMLElement = screen.getByText(/25:00/i);
      expect(particularTime25Min).toBeInTheDocument();
      done();
    } catch (error) {
      done(error);
    }
  }, 2000); // NOTE: Wait more than a second...
});
