import { PomodoroState, Action, ActionType } from "./Typing";
import { initPomodoroState } from "./Init";
import WORKING_STATUSES from "./WorkingStatus";

function reducer(state: PomodoroState, action: Action): PomodoroState {
  const newElapsed = Date.now() - state.lastRecordedTime;
  switch (action.type) {
    case ActionType.START:
      if (
        state.workingStatus === WORKING_STATUSES.notWorking ||
        state.workingStatus === WORKING_STATUSES.paused
      ) {
        if (!state.timeStarted) {
          return {
            ...state,
            running: true,
            workingStatus: WORKING_STATUSES.defaultDirty,
            timeStarted: Date.now(),
            lastRecordedTime: Date.now(),
          };
        }
        return {
          ...state,
          running: true,
          workingStatus: WORKING_STATUSES.defaultDirty,
          lastRecordedTime: Date.now(),
        };
      }
      return state;

    case ActionType.PAUSE:
      if (!state.running) {
        return state;
      }
      return {
        ...state,
        workingStatus: WORKING_STATUSES.paused,
        lastRecordedTime: Date.now(),
        lastRecordedElapsed: state.lastRecordedElapsed + newElapsed,
        running: false,
      };

    case ActionType.RESET:
      return initPomodoroState(state.sessionType);

    case ActionType.TOGGLE_WORKING_STATUS:
      if (state.workingStatus === WORKING_STATUSES.working) {
        return {
          ...state,
          workingStatus: WORKING_STATUSES.planning,
        };
      }
      if (state.workingStatus === WORKING_STATUSES.planning) {
        return {
          ...state,
          workingStatus: WORKING_STATUSES.working,
        };
      }
      return state;
    case ActionType.CHANGE_PLAN:
      return {
        ...state,
        planningText: action.payload,
      };

    case ActionType.CHANGE_TIMER_MODE:
      return initPomodoroState(action.payload);

    default:
      throw new Error();
  }
}

export default reducer;
