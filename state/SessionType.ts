import { SessionType } from "./Typing";

const SESSION_TYPES = {
  pomodoro: SessionType.POMODORO,
  shortBreak: SessionType.SHORT_BREAK,
  longBreak: SessionType.LONG_BREAK,
};

export default SESSION_TYPES;
