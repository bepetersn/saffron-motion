import { WorkingStatus } from "./Typing";

const WORKING_STATUSES = {
  paused: WorkingStatus.PAUSED,
  working: WorkingStatus.WORKING,
  planning: WorkingStatus.PLANNING,
  notWorking: WorkingStatus.NOT_WORKING,
  defaultDirty: WorkingStatus.PLANNING,
};

export default WORKING_STATUSES;
