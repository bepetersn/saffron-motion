export enum SessionType {
  POMODORO = "Pomodoro",
  SHORT_BREAK = "Short Break",
  LONG_BREAK = "Long Break",
}

export enum WorkingStatus {
  PAUSED = "Paused",
  WORKING = "Working",
  PLANNING = "Planning",
  NOT_WORKING = "Not working",
}

export type PomodoroState = {
  running: boolean;
  timeStarted: number;
  lastRecordedTime: number;
  lastRecordedElapsed: number;
  workingStatus: WorkingStatus;
  sessionType: SessionType;
  planningText: string;
  tick: number;
};

export type TimerState = Pick<
  PomodoroState,
  | "running"
  | "timeStarted"
  | "lastRecordedTime"
  | "lastRecordedElapsed"
  | "sessionType"
>;

export enum ActionType {
  START = "START",
  PAUSE = "PAUSE",
  RESET = "RESET",
  TOGGLE_WORKING_STATUS = "TOGGLE_WORKING_STATUS",
  CHANGE_PLAN = "CHANGE_PLAN",
  CHANGE_TIMER_MODE = "CHANGE_TIMER_MODE",
}

export type Action =
  | { type: ActionType.START }
  | { type: ActionType.PAUSE }
  | { type: ActionType.RESET }
  | { type: ActionType.TOGGLE_WORKING_STATUS }
  | { type: ActionType.CHANGE_PLAN; payload: string }
  | { type: ActionType.CHANGE_TIMER_MODE; payload: SessionType };
