import { PomodoroState, SessionType } from "./Typing";
import WORKING_STATUSES from "./WorkingStatus";

export function initPomodoroState(sessionType: SessionType): PomodoroState {
  return {
    sessionType: sessionType,
    workingStatus: WORKING_STATUSES.notWorking,
    planningText: "",
    running: false,
    timeStarted: 0,
    lastRecordedTime: 0,
    lastRecordedElapsed: 0,
    tick: 0,
  };
}
