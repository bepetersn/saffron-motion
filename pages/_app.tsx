import "../styles/globals.css";
import React, { ReactElement } from "react";
import type { AppProps } from "next/app";
import { ChakraProvider } from "@chakra-ui/react";

type WebVitals = {
  id: string;
  name: string;
  delta: number;
  value: number;
};

export function reportWebVitals(metric: WebVitals): void {
  console.log(metric);
}

function App({ Component, pageProps }: AppProps): ReactElement {
  return (
    <ChakraProvider>
      <Component {...pageProps} />
    </ChakraProvider>
  );
}

export default App;
