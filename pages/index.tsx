import { ReactElement } from "react";
import IndexPage from "../components/IndexPage";

export default function Index(): ReactElement {
  return <IndexPage />;
}
