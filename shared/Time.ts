import { TimerState } from "../state/Typing";
import SESSION_TYPES from "../state/SessionType";

// Below: n minutes in milliseconds (n * 60 * 1000)
export const INITIAL_TIME_IN_MILLIS = {
  [SESSION_TYPES.pomodoro]: 25 * 60 * 1000,
  [SESSION_TYPES.shortBreak]: 5 * 60 * 1000,
  [SESSION_TYPES.longBreak]: 15 * 60 * 1000,
};

export function parseDateDiff(dateDiffStr: string): number {
  /* convert from mm:ss to diff */
  const [minutesStr, secondsStr] = dateDiffStr.split(":");
  const [minutes, seconds] = [
    parseInt(minutesStr, 10),
    parseInt(secondsStr, 10),
  ];
  return minutes * 60 * 1000 + seconds * 1000;
}

export function formatDateDiff(dateDiff: number): string {
  /* convert diff to mm:ss format */
  const minutes = Math.floor(dateDiff / (1000 * 60));
  const seconds = Math.floor(
    dateDiff / 1000 - minutes * 60 // eslint-disable-line comma-dangle
  );
  const minutesStr = String(minutes).padStart(2, "0");
  const secondsStr = String(seconds).padStart(2, "0");
  return `${minutesStr}:${secondsStr}`;
}

export function calculateTimeRemaining(timerState: TimerState): number {
  const { lastRecordedTime, lastRecordedElapsed, sessionType } = timerState;
  const initialTime = INITIAL_TIME_IN_MILLIS[sessionType];
  const lastRecorded = lastRecordedTime || Date.now();
  const newElapsed = Date.now() - lastRecorded;
  const totalElapsed = lastRecordedElapsed + newElapsed;
  if (totalElapsed < initialTime) {
    return initialTime - totalElapsed;
  }
  return 0;
}

export function getFormattedTimeRemaining(timerState: TimerState): string {
  return formatDateDiff(calculateTimeRemaining(timerState));
}
