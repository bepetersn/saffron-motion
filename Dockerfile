FROM node:current-alpine

EXPOSE 5000
WORKDIR /root
ADD . .
RUN yarn install && yarn build

CMD yarn serve -s build
